import { call, put, select, fork } from 'redux-saga/effects';

import {
  REQUEST_POKEMONS_STARTED,
  REQUEST_POKEMONS_FINISHED,
  REQUEST_POKEMON_STARTED,
  REQUEST_POKEMON_FINISHED,
  SELECT_POKEMON,
  NEXT_PAGE,
} from '../Actions/pokemon';
import { fetchPokemonList, fetchPokemon } from '../api';

export function* selectPokemon(action) {
  const pokemon = yield select(state => state.pokemonDetails[action.name]);

  if (pokemon) {
    yield put({
      type: SELECT_POKEMON,
      pokemon
    });
  } else {
    yield fork(requestPokemon, action);
  }
}

export function* requestPokemons(action) {
  yield put({
    type: REQUEST_POKEMONS_STARTED
  });

  const response = yield call(fetchPokemonList, action.url);

  yield put({
    type: REQUEST_POKEMONS_FINISHED,
    pokemon: response.results,
    nextPageUrl: response.next,
  });
}

export function* requestPokemon(action) {
  yield put({
    type: REQUEST_POKEMON_STARTED
  });

  const pokemon = yield call(fetchPokemon, action.url);

  yield put({
    type: REQUEST_POKEMON_FINISHED,
    pokemon,
  });
}

export function* nextPage(action) {
  const meta = yield select(state => ({
    pokemonCount: state.pokemon.length,
    page: state.currentPage,
    offset: state.offset,
  }));

  if (meta.pokemonCount === (meta.page * meta.offset)) {
    yield fork(requestPokemons, { url: action.url });
  } else {
    yield put({ type: NEXT_PAGE });
  }
}
