import { all, fork, takeLatest } from 'redux-saga/effects';

import { REQUEST_POKEMONS, REQUEST_POKEMON, GET_NEXT_PAGE } from '../Actions/pokemon';
import { requestPokemons, selectPokemon, nextPage } from './pokemon';

export default function* sagas() {
  yield all([
    fork(takeLatest, REQUEST_POKEMONS, requestPokemons),
    fork(takeLatest, REQUEST_POKEMON, selectPokemon),
    fork(takeLatest, GET_NEXT_PAGE, nextPage),
  ]);
}