import React from 'react';
import { shallow } from 'enzyme';
import { PokemonList } from './PokemonList';
import { Route } from 'react-router-dom';

import PokemonListElement from './PokemonListElement/PokemonListElement';

let wrapper;

const props = {
  pokemon: [
    {
      name: 'Test 1',
      url: 'Url 1',
    },
    {
      name: 'Test 2',
      url: 'Url 2',
    },
    {
      name: 'Test 3',
      url: 'Url 3',
    }
  ],
  hasPreviousPage: true,
  nextPageUrl: 'url',
  dispatch: () => {},
}

describe('PokemonList', () => {
  beforeEach(() => {
    wrapper = shallow(
      <PokemonList {...props}/>
    );
  });

  test('should render Pokemon List', () => {
    expect(wrapper.find(PokemonListElement).length).toEqual(3);
  });

  test('should render pagination', () => {
    expect(wrapper.containsMatchingElement(<button>Previous</button>)).toBe(true);
    expect(wrapper.containsMatchingElement(<button>Next</button>)).toBe(true);
  });
});