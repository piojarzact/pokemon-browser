import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  requestPokemons,
  selectPokemon,
  getMorePokemon,
  previousPage
} from '../../Actions/pokemon';

import PokemonListElement from './PokemonListElement/PokemonListElement';

import './PokemonList.css';
export class PokemonList extends Component {
  constructor(props) {
    super(props);

    this.clickHandler = this.clickHandler.bind(this);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(requestPokemons());
  }

  clickHandler(pokemonName, pokemonUrl) {
    this.props.dispatch(selectPokemon(pokemonName, pokemonUrl));
  }

  nextPage() {
    this.props.dispatch(getMorePokemon(this.props.nextPageUrl));
  }

  previousPage() {
    this.props.dispatch(previousPage());
  }

  render() {
    if (this.props.pokemonsLoading) {
      return <div className='loading'>Loading...</div>
    }

    if (this.props.pokemon.length) {
      return (
        <div>
          <ul className='list-group'>
            {this.props.pokemon.map(pokemon => (
              <PokemonListElement
                key={`pokemon-list-${pokemon.name}`}
                name={pokemon.name}
                url={pokemon.url}
                handleClick={this.clickHandler}
              />
            ))}
          </ul>

          <div>
            {this.props.hasPreviousPage && <button className='btn btn-primary prev' onClick={this.previousPage}>Previous</button>}
            {this.props.nextPageUrl && <button className='btn btn-primary next' onClick={this.nextPage}>Next</button>}
          </div>
        </div>
      );
    }
    
    return (<span>Sorry, no pokemons :(</span>);
  }
}

function mapStateToProps(state) {
  return {
    pokemon: state.pokemon.slice((state.currentPage - 1) * state.offset, state.currentPage * state.offset) || [],
    pokemonsLoading: state.pokemonsLoading || false,
    nextPageUrl: state.nextPageUrl || null,
    hasPreviousPage: state.currentPage > 1,
  };
};

export default connect(mapStateToProps)(PokemonList);
