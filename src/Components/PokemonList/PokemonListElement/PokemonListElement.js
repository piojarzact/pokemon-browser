import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import './PokemonListElement.css';

class PokemonListElement extends Component {
  constructor(props) {
    super(props);

    this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler(e) {
    this.props.handleClick(this.props.name, this.props.url);
  }

  render() {
    return (<li className='list-group-item list-group-item-action text-capitalize'>
        <NavLink to={`/pokemon/${this.props.name}`} activeClassName="active" onClick={this.clickHandler}>{this.props.name}</NavLink>
      </li>);
  }
}
// <button onClick={this.clickHandler}>{this.props.name}</button>
export default PokemonListElement;
