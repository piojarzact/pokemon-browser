import React from 'react';
import { shallow } from 'enzyme';
import PokemonAbilities from './PokemonAbilities';

const props = {
    abilities: [
        {
            ability: {
                name: 'Test ability 1',
            },
        },
        {
            ability: {
                name: 'Test ability 2',
            },
        },
    ]
};
let wrapper;

describe('PokemonAbilities', () => {
  beforeEach(() => {
    wrapper = shallow(
      <PokemonAbilities {...props}/>
    );
  });

  test('should render abilities', () => {
    expect(wrapper.find('li').length).toEqual(2);
  });
});
    