import React from 'react';

const PokemonAbilities = ({abilities}) => (
    <div className='pokemon-abilities'>
        <h4>Abilities</h4>
        <ul className='list-unstyled'>
        {abilities.map((ability, index) => (
            <li key={index} className='text-capitalize'>{ability.ability.name}</li>
        ))}
        </ul>
    </div>
);

export default PokemonAbilities;
    