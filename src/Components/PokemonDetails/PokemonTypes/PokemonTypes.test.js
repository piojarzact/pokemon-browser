import React from 'react';
import { shallow } from 'enzyme';
import PokemonTypes from './PokemonTypes';

const props = {
    types: [
        {
            type: {
                name: 'Test type 1',
            },
        },
        {
            type: {
                name: 'Test type 2',
            },
        },
    ]
};
let wrapper;

describe('PokemonTypes', () => {
  beforeEach(() => {
    wrapper = shallow(
      <PokemonTypes {...props}/>
    );
  });

  test('should render stats', () => {
    expect(wrapper.find('.pokemon-types').length).toEqual(1);
    expect(wrapper.find('.pokemon-types span').length).toEqual(2);
  });
});
    