import React from 'react';

import './PokemonTypes.css';

const PokemonTypes = ({types}) => (
    <div className='row pokemon-types'>
        <h5>
        {types.length === 1 && 'Type:'}
        {types.length > 1 && 'Types:'}
        {types.map((type, index) => (
            <span className='text-capitalize' key={index}> {type.type.name}</span>
        ))}
        </h5>
    </div>
);

export default PokemonTypes;
    