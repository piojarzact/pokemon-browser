import React from 'react';
import { shallow } from 'enzyme';
import PokemonStats from './PokemonStats';

const props = {
    stats: [
        {
            stat: {
                name: 'Test stat 1',
                base_stat: true,
                effort: 50,
            },
        },
        {
            stat: {
                name: 'Test stat 2',
                base_stat: false,
                effort: 150,
            },
        },
    ]
};
let wrapper;

describe('PokemonStats', () => {
  beforeEach(() => {
    wrapper = shallow(
      <PokemonStats {...props}/>
    );
  });

  test('should render stats', () => {
    expect(wrapper.find('.pokemon-stat').length).toEqual(2);
    expect(wrapper.find('.pokemon-stat span').length).toEqual(2);
    expect(wrapper.find('.pokemon-stat dl').length).toEqual(2);
  });
});
    