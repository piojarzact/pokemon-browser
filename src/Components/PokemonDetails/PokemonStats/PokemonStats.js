import React from 'react';

const PokemonStats = ({stats}) => (
    <div className='pokemon-stats'>
        <h4>Stats</h4>
        {stats.map((stat, index) => (
            <div key={index} className='pokemon-stat'>
                <span className='text-capitalize'>{stat.stat.name}</span><br/>
                <dl className='row'>
                    <dt className='col-sm-6'>Base stat</dt>
                    <dd className='col-sm-6'>{stat.base_stat}</dd>
                    <dt className='col-sm-6'>Effort</dt>
                    <dd className='col-sm-6'>{stat.effort}</dd>
                </dl>
            </div>
        ))}
    </div>
);

export default PokemonStats;
    