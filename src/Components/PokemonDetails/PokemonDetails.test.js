import React from 'react';
import { shallow } from 'enzyme';
import { PokemonDetails } from './PokemonDetails';
import { Route } from 'react-router-dom';

import PokemonAbilities from './PokemonAbilities/PokemonAbilities';
import PokemonSprite from './PokemonSprite/PokemonSprite';
import PokemonStats from './PokemonStats/PokemonStats';
import PokemonTypes from './PokemonTypes/PokemonTypes';
let wrapper;

const props = {
  selectedPokemon: {
    name: 'Test',
    height: 50,
    weight: 30,
    base_experience: 100,
  }
}

describe('PokemonDetails', () => {
  beforeEach(() => {
    wrapper = shallow(
      <PokemonDetails {...props}/>
    );
  });

  test('should render basic Pokemon deatils', () => {
    expect(wrapper.contains(<h1 className='text-capitalize'>{props.selectedPokemon.name}</h1>)).toBe(true);
    expect(wrapper.contains(
      <div className='col-sm-3'>
        <dl className='row'>
          <dt className='col-sm-6'>Height</dt>
          <dd className='col-sm-6'>{props.selectedPokemon.height}</dd>
          <dt className='col-sm-6'>Weight</dt>
          <dd className='col-sm-6'>{props.selectedPokemon.weight}</dd>
          <dt className='col-sm-6'>Base experience</dt>
          <dd className='col-sm-6'>{props.selectedPokemon.base_experience}</dd>
        </dl>
      </div>
    )).toBe(true);
  });

  test('should render Pokemon types, sprites, stats, and abilities', () => {
    expect(wrapper.find(PokemonAbilities).length).toEqual(1);
    expect(wrapper.find(PokemonSprite).length).toEqual(1);
    expect(wrapper.find(PokemonStats).length).toEqual(1);
    expect(wrapper.find(PokemonTypes).length).toEqual(1);
  });
});