import React, { Component } from 'react';
import { connect } from 'react-redux';

import PokemonTypes from './PokemonTypes/PokemonTypes';
import PokemonSprite from './PokemonSprite/PokemonSprite';
import PokemonStats from './PokemonStats/PokemonStats';
import PokemonAbilities from './PokemonAbilities/PokemonAbilities';

import './PokemonDetails.css';

export class PokemonDetails extends Component {
  render() {
    if (this.props.pokemonLoading) {
      return <div className='loading'>Loading...</div>
    }

    if (this.props.selectedPokemon) {
      return (
        <section>

                <h1 className='text-capitalize'>{this.props.selectedPokemon.name}</h1>

                <PokemonTypes types={this.props.selectedPokemon.types}/>

                <div className='row'>
                  <div className='col-sm-3'>
                    <dl className='row'>
                        <dt className='col-sm-6'>Height</dt>
                        <dd className='col-sm-6'>{this.props.selectedPokemon.height}</dd>
                        <dt className='col-sm-6'>Weight</dt>
                        <dd className='col-sm-6'>{this.props.selectedPokemon.weight}</dd>
                        <dt className='col-sm-6'>Base experience</dt>
                        <dd className='col-sm-6'>{this.props.selectedPokemon.base_experience}</dd>
                    </dl>
                  </div>
                  <div className='col-sm-8'>
                    <PokemonSprite sprites={this.props.selectedPokemon.sprites}/>
                  </div>
                </div>
            <div className='row'>
              <div className='col-sm-3'>
                <PokemonStats stats={this.props.selectedPokemon.stats}/>
              </div>
              <div className='col-sm-6'>
                <PokemonAbilities abilities={this.props.selectedPokemon.abilities}/>
              </div>
            </div>

        </section>
      );
    }
    
    return (<h3 className='text-center'>Select a Pokemon from the list on the left to see some details about it!</h3>);
  }
}

function mapStateToProps(state) {
  return {
    selectedPokemon: state.selectedPokemon || null,
    pokemonLoading: state.pokemonLoading || false,
  };
};

export default connect(mapStateToProps)(PokemonDetails);
