import React from 'react';
import { mount } from 'enzyme';
import PokemonSprite from './PokemonSprite';

describe('PokemonSprite', () => {
  test('should render all sprites if available', () => {
    let props = {
        sprites: {
            front_default: 'front_default',
            front_female: 'front_female',
            front_shiny: 'front_shiny',
            front_shiny_female: 'front_shiny_female',
            back_default: 'back_default',
            back_female: 'back_female',
            back_shiny: 'back_shiny',
            back_shiny_female: 'back_shiny_female',
        },
    };
    let wrapper = mount(
        <PokemonSprite {...props}/>
    );
    
    expect(wrapper.find('div').length).toEqual(2)
    expect(wrapper.find('div img').length).toEqual(8);
    
  });

  test('should render only available sprites', () => {
    let props = {
        sprites: {
            front_default: 'front_default',
            front_shiny_female: 'front_shiny_female',
        },
    };
    let wrapper = mount(
        <PokemonSprite {...props}/>
    );
    
    expect(wrapper.find('div').length).toEqual(2)
    expect(wrapper.find('div img').length).toEqual(2);
  });
});
    