import React from 'react';

const SPRITES = [
    ['front_default', 'front_female', 'front_shiny', 'front_shiny_female'],
    ['back_default', 'back_female', 'back_shiny', 'back_shiny_female']
];

const PokemonSprite = ({sprites}) => (
    SPRITES.map((position, index) => (
        <div key={index}>
            {position.map((key, idx) => sprites[key] ? <img key={idx} src={sprites[key]} alt={`POKEMON NAME ${key}`} /> : '')}
        </div>
    ))
);

export default PokemonSprite;