import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import './App.css';

import PokemonList from '../PokemonList/PokemonList';
import PokemonDetails from '../PokemonDetails/PokemonDetails';

class App extends Component {
  render() {
    return (
      <div className='app-container '>
        <div className='header'>
          <h1>Pokemon Browser</h1>
        </div>
        <div className='left-pane'>
          <PokemonList />
        </div>
        <div className='right-pane'>
          <Route path={`/pokemon/:pokemon`} component={PokemonDetails}/>
        </div>
      </div>
    );
  }
}

export default App;