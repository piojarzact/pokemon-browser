import React from 'react';
import { shallow } from 'enzyme';
import App from './App';
import { Route } from 'react-router-dom';

import PokemonList from '../PokemonList/PokemonList';


let wrapper;

describe('App', () => {
  beforeEach(() => {
    wrapper = shallow(
      <App/>
    );
  });

  test('should have title', () => {
    expect(wrapper.contains(<h1>Pokemon Browser</h1>)).toBe(true);
  });

  test('should Pokemon list and details view', () => {
    expect(wrapper.find(PokemonList).length).toEqual(1);
    expect(wrapper.find(Route).length).toEqual(1);
  });
});