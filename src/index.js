import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga'
import { BrowserRouter, Route } from 'react-router-dom';

import './index.css';
import App from './Components/App/App';
import registerServiceWorker from './registerServiceWorker';

import pokemonReducer from './Reducers/pokemon';
import sagas from './Sagas';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    pokemonReducer,
    composeEnhancers(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(sagas);

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <Route path="/" component={App}/>
        </Provider>
    </BrowserRouter>,
    document.getElementById('root')
);
registerServiceWorker();
