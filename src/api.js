import axios from 'axios';

export function fetchPokemonList(url = 'https://pokeapi.co/api/v2/pokemon/') {
  return axios.get(url)
    .then(response => response.data)
};

export function fetchPokemon(url) {
  return axios.get(url)
    .then(response => response.data)
};
  