import React from 'react';
import { shallow } from 'enzyme';

import * as actions from '../Actions/pokemon'
import reducer from './pokemon';

describe('Pokemon reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        pokemon: [],
        selectedPokemon: null,
        pokemonDetails: {},
        pokemonsLoading: false,
        pokemonLoading: false,
        currentPage: 0,
        offset: 10,
      }
    )
  })

  it('should handle REQUEST_POKEMONS_STARTED', () => {
    expect(
      reducer({}, {
        type: actions.REQUEST_POKEMONS_STARTED,
      })
    ).toEqual(
      {
        pokemonsLoading: true,
      }
    )
  })

  it('should handle REQUEST_POKEMONS_FINISHED', () => {
    expect(
      reducer({
        pokemon: ['Initial'],
        currentPage: 15,
      }, {
        type: actions.REQUEST_POKEMONS_FINISHED,
        pokemon: ['test', 'test'],
        nextPageUrl: 'url'
      })
    ).toEqual(
      {
        pokemon: ['Initial', 'test', 'test'],
        nextPageUrl: 'url',
        currentPage: 16,
        pokemonsLoading: false,
      }
    )
  })


  it('should handle REQUEST_POKEMON_STARTED', () => {
    expect(
      reducer({}, {
        type: actions.REQUEST_POKEMON_STARTED,
      })
    ).toEqual(
      {
        pokemonLoading: true,
      }
    )
  })


  it('should handle REQUEST_POKEMON_FINISHED', () => {
    expect(
      reducer({
        pokemonDetails: {
          'initial': 'initial'
        }
      }, {
        type: actions.REQUEST_POKEMON_FINISHED,
        pokemon: {
          name: 'test',
        }
      })
    ).toEqual(
      {
        selectedPokemon: {
          name: 'test',
        },
        pokemonDetails: {
          initial: 'initial',
          test: {
            name: 'test',
          }
        },
        pokemonLoading: false,
      }
    )
  })


  it('should handle SELECT_POKEMON', () => {
    expect(
      reducer({}, {
        type: actions.SELECT_POKEMON,
        pokemon: {
          name: 'test',
        }
      })
    ).toEqual(
      {
        selectedPokemon: {
          name: 'test',
        },
      }
    )
  })

  it('should handle NEXT_PAGE', () => {
    expect(
      reducer({
        currentPage: 10,
      }, {
        type: actions.NEXT_PAGE,
      })
    ).toEqual(
      {
        currentPage: 11,
      }
    )
  })

  it('should handle PREVIOUS_PAGE', () => {
    expect(
      reducer({
        currentPage: 10,
      }, {
        type: actions.PREVIOUS_PAGE,
      })
    ).toEqual(
      {
        currentPage: 9,
      }
    )
  })
})