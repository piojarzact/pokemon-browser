import {
  REQUEST_POKEMONS_STARTED,
  REQUEST_POKEMONS_FINISHED,
  REQUEST_POKEMON_STARTED,
  REQUEST_POKEMON_FINISHED,
  SELECT_POKEMON,
  NEXT_PAGE,
  PREVIOUS_PAGE,
} from '../Actions/pokemon';

const INITIAL_STATE = {
  pokemon: [],
  selectedPokemon: null,
  pokemonDetails: {},
  pokemonsLoading: false,
  pokemonLoading: false,
  currentPage: 0,
  offset: 10,
};

export default function pokemon(state = INITIAL_STATE, action) {
  switch (action.type) {
    case REQUEST_POKEMONS_STARTED:
      return Object.assign({}, state, {
        pokemonsLoading: true,
      });
    case REQUEST_POKEMONS_FINISHED:
      return Object.assign({}, state, {
          pokemon: [...state.pokemon, ...action.pokemon],
          nextPageUrl: action.nextPageUrl,
          currentPage: state.currentPage + 1,
          pokemonsLoading: false,
        });
    case REQUEST_POKEMON_STARTED:
      return Object.assign({}, state, {
        pokemonLoading: true,
      });
    case REQUEST_POKEMON_FINISHED:
      return Object.assign({}, state, {
        selectedPokemon: action.pokemon,
        pokemonDetails: Object.assign(
          {},
          state.pokemonDetails,
          {
            [action.pokemon.name]: action.pokemon
          }
        ),
        pokemonLoading: false,
      });
    case SELECT_POKEMON:
      return Object.assign({}, state, {
        selectedPokemon: action.pokemon,
      });
    case NEXT_PAGE:
      return Object.assign({}, state, {
        currentPage: state.currentPage + 1,
      });
    case PREVIOUS_PAGE:
      return Object.assign({}, state, {
        currentPage: state.currentPage - 1,
      });
    default:
      return state;
  }
};