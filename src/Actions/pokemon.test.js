import React from 'react';
import { shallow } from 'enzyme';

import * as actions from './Pokemon'

describe('actions', () => {
  it('should create an action to request Pokemons from API with default page', () => {
    const expectedAction = {
      type: actions.REQUEST_POKEMONS,
      page: 1,
    }
    expect(actions.requestPokemons()).toEqual(expectedAction)
  });

  it('should create an action to request Pokemons from API with provided page', () => {
    const expectedAction = {
      type: actions.REQUEST_POKEMONS,
      page: 14,
    }
    expect(actions.requestPokemons(14)).toEqual(expectedAction)
  });

  it('should create an action to select Pokemons with default name and url', () => {
    const expectedAction = {
      type: actions.REQUEST_POKEMON,
      name: '',
      url: ''
    }
    expect(actions.selectPokemon()).toEqual(expectedAction)
  });

  it('should create an action to select Pokemons with name and url', () => {
    const expectedAction = {
      type: actions.REQUEST_POKEMON,
      name: 'pokemon test',
      url: 'some url'
    }
    expect(actions.selectPokemon('pokemon test', 'some url')).toEqual(expectedAction)
  });

  it('should create an action to get next page of Pokemons with default url', () => {
    const expectedAction = {
      type: actions.GET_NEXT_PAGE,
      url: ''
    }
    expect(actions.getMorePokemon()).toEqual(expectedAction)
  });

  it('should create an action to get next page of Pokemons with provided url', () => {
    const expectedAction = {
      type: actions.GET_NEXT_PAGE,
      url: 'some url'
    }
    expect(actions.getMorePokemon('some url')).toEqual(expectedAction)
  });

  it('should create an action to get revious page of Pokemons with default url', () => {
    const expectedAction = {
      type: actions.PREVIOUS_PAGE,
    }
    expect(actions.previousPage()).toEqual(expectedAction)
  });
})