export const REQUEST_POKEMONS = 'REQUEST_POKEMONS';
export const REQUEST_POKEMONS_STARTED = 'REQUEST_POKEMONS_STARTED';
export const REQUEST_POKEMONS_FINISHED = 'REQUEST_POKEMONS_FINISHED';

export const REQUEST_POKEMON = 'REQUEST_POKEMON';
export const REQUEST_POKEMON_STARTED = 'REQUEST_POKEMON_STARTED';
export const REQUEST_POKEMON_FINISHED = 'REQUEST_POKEMON_FINISHED';

export const SELECT_POKEMON = 'SELECT_POKEMON';
export const GET_NEXT_PAGE = 'GET_NEXT_PAGE';
export const NEXT_PAGE = 'NEXT_PAGE';
export const PREVIOUS_PAGE = 'PREVIOUS_PAGE';


export function requestPokemons(page = 1) {
    return {
        type: REQUEST_POKEMONS,
        page,
    };
};

export function selectPokemon(name = '', url = '') {
    return {
        type: REQUEST_POKEMON,
        name,
        url,
    };
};

export function getMorePokemon(url = '') {
    return {
        type: GET_NEXT_PAGE,
        url,
    };
};

export const previousPage = () => ({ type: PREVIOUS_PAGE });
