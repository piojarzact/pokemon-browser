### Installation

This app requires [Node.js](https://nodejs.org/) and [Yarn](https://yarnpkg.com/en/) (or npm) to run.

Install the dependencies and start the server.

```sh
$ cd pokemon-browser
$ yarn
$ yarn start
```
or
```sh
$ cd pokemon-browser
$ npm install -d
$ npm start
```
### Testing
To run unit tests

```sh
$ yarn test
```
or
```sh
$ npm test
```

### Description
This is a simple appilication showing a list of Pokemons and basic set of details of one selected Pokemon. 

App was created by using **create-react-app**, and it was ejected to support **Enzyme** configuration files.

[PokeApi](http://pokeapi.co/) was used as provider for all the data and **Axios** was chosen as a request library. Native fetch could've been used for this functionality, but the used library handles some cases (for example request errors handling) out of the box, unlike fetch.

**Redux** was used as a state container, with **Redux Saga** taking care of async actions.

Basic routing and navigation handling was implemented by using **React Router v4**.

Both **Redux Saga** and current version of **React Router** were chosen because I didn't have a chance to work with those yet and I wanted to try them out.

Unit tests were written with help of **Jest** and **Enzyme**. **Jest** was not changed to anything else as it's part of **create-react-app**. But as the app was ejected it could be switched to any other JS testing library. **Enzyme** was added to ease testing of React components.

All components files (main JS file, styles and test) are kept in the same directory. Whether such approach makes sense or not is discussable, but for such a small project it did not matter. Those could be seperated if the app grew bigger.

### Areas to be improved
Handle loading of selected Pokemon detail on page refresh

Add PropTypes to components

Don't use indices as keys for components in lists

Consume more data from the API

Enhance UI in both styling and UX

Cache API response (for example in localStorage) as it should not change very often

Add error handling for the API requests

Move common UI parts to their own component (for example loading and empty views)

More unit and other types of tests should be added, as well as test coverage metrics.